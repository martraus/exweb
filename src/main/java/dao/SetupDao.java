package dao;

import exweb.Post;
import util.DataSourceProvider;
import util.FileUtil;
import util.PropertyLoader;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class SetupDao {

    public void initDatabase() {

        String dbUrl = PropertyLoader.getProperty("jdbc.url");

        DataSourceProvider.setDbUrl(dbUrl);

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             Statement stmt = conn.createStatement()) {

            stmt.executeQuery(FileUtil.readFileFromClasspath("schema.sql"));
            stmt.executeQuery(FileUtil.readFileFromClasspath("data.sql"));

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
