package dao;

import exweb.Post;
import util.DataSourceProvider;
import util.PropertyLoader;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostDao {

    public PostDao() {
        String dbUrl = PropertyLoader.getProperty("jdbc.url");

        DataSourceProvider.setDbUrl(dbUrl);
    }

    public List<Post> getPosts() {

        List<Post> posts = new ArrayList<>();

        try (Connection conn = DataSourceProvider.getDataSource().getConnection();
             Statement stmt = conn.createStatement()) {

            String sql = "select id, title, text from post";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                posts.add(new Post(rs.getLong("id"),
                        rs.getString("title"),
                        rs.getString("text")));
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return posts;
    }

}
